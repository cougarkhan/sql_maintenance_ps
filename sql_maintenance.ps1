#####################################################################################
#
# Version 1.05
#
# Written on 03/08/2012
# By Michael Pal
# 
#####################################################################################
$scriptpath = $MyInvocation.MyCommand.Path
$scriptpath = Split-Path $scriptpath

. $scriptpath\functions\format_date.ps1
. $scriptpath\functions\db_functions.ps1
. $scriptpath\functions\sql_db_functions.ps1

$localhost = (gci env:COMPUTERNAME).value
$errorResult = @{}
$errorCatastrophic = $false
$logDate = format_date
$logTime = format_time
$log = create_log "C:\Temp\$localhost-$logTime-$logDate.txt"
if (test-path "$scriptpath\etc\emailconfig.xml") {
	$email = [xml](gc "$scriptpath\etc\emailconfig.xml")
	$sender = $email.email.sender
	$recipients = $email.email.recipients
} else {
	$errorResult.Add("Email Configuration","Email Configuration file not found at $scriptpath\etc\emailconfig.xml!")
	$sender = "$localhost@bussops.ubc.ca"
	$recipients = "bops.systems@ubc.ca"
}
if (test-path "$scriptpath\etc\$localhost.xml") {
	$settings = [xml](gc "$scriptpath\etc\$localhost.xml")
	$server = $settings.config.sqlserver.instance.name
	[array]$databases = $settings.config.sqlserver.databases.name
	$options = $settings.config.sqlserver.options
	$checkIntegrity = $options.checkIntegrity.enabled
	$indexDefrag = $options.indexDefrag.enabled
	$updateStatistics = $options.updateStatistics.enabled
	$backupDatabase = $options.backupDatabase.enabled
	$backupLocation = $options.backupDatabase.location
	$moveFiles = $options.movefiles.enabled
	$moveFileSource = $options.movefiles.sourcefolder
	$moveFileTarget = $options.movefiles.targetfolder
	$cleanupFiles = $options.cleanupFiles.enabled
	$cleanupFileSource = $options.cleanupFiles.sourcefolder
	$cleanupFileAgeDays = $options.cleanupFiles.fileagedays
	$compression = $options.compression.enabled
	$backupResult = $false
	$cleanupFilesResult = $false
	$checkIntegrityResult = $false
	$log = create_log "$backupLocation\$localhost-$logTime-$logDate.txt"
} else {
	$errorResult.Add("Global Configuration File","Global Configuration File not found at $scriptpath\etc\$localhost.xml")
	$errorCatastrophic = $true
}

if ($errorCatastrophic -eq $false) {

	Start-Transcript -Path $log

	Trap {
		$err = $_.Exception
		while ( $err.InnerException )
			{
			$err = $err.InnerException
			write-output $err.Message
		};
		continue
	}

	Write-Host "`n--------------------------------------------"
	Write-Host "Loading SQL Assemblies..."
	Write-Host "--------------------------------------------`n"
	$errorResult.Add("Microsoft.SqlServer.SMO", (load_assemblies "Microsoft.SqlServer.SMO"))
	$errorResult.Add("Microsoft.SqlServer.SmoExtended", (load_assemblies "Microsoft.SqlServer.SmoExtended"))
	$errorResult.Add("Microsoft.SqlServer.ConnectionInfo", (load_assemblies "Microsoft.SqlServer.ConnectionInfo"))
	$errorResult.Add("Microsoft.SqlServer.SqlEnum", (load_assemblies "Microsoft.SqlServer.SqlEnum"))


	Write-Host (log_time)"`tCreating Sql Server Object " -nonewline
	Write-Host "$server..." -nonewline
	$timeResult = measure-command {
		$svr = new-object "Microsoft.SqlServer.Management.SMO.Server" $server
	}
	Write-Host "Completed. $timeResult"
	
	$svr.ConnectionContext.StatementTimeout = 3600

	$sqlVer = $svr.VersionString.SubString(0,4)

	Write-host "`nProcessing $server SQL Version"$svr.VersionString"`n"

	Write-Host "`n--------------------------------------------"
	Write-Host "Skipped Operations..."
	Write-Host "--------------------------------------------`n"
	if ($cleanupFiles -eq 0) { Write-Host (log_time)"`tSkipping Cleaning up of Database files." }
	if ($checkIntegrity -eq 0) { Write-Host (log_time)"`tSkipping Table Ingerity checks." }
	if ($indexDefrag -eq 0) { Write-Host (log_time)"`tSkipping Index Deframenting." }
	if ($updateStatistics -eq 0) { Write-Host (log_time)"`tSkipping Updating Statistics." }
	if ($backupDatabase -eq 0) { Write-Host (log_time)"`tSkipping Backing up of Database." }
	if ($moveFiles -eq 0) { Write-Host (log_time)"`tSkipping Move of Database Files." }

	[array]$dbs = @()

	$databases | %{ 
		$tempDB = new-object "Microsoft.SqlServer.Management.SMO.Database"
		$tempDB = $svr.Databases[$_]
		$dbs += $tempDB
	}

	if ($cleanupFiles -eq 1) {
		Write-Host "`n--------------------------------------------"
		Write-Host "Performing Cleanup Operations..."
		Write-Host "--------------------------------------------`n"
		foreach ($db in $dbs) {
			$dbName = $db.Name
			$folder = "$cleanupFileSource\$dbName\"
			$errorResult.Add("Cleanup Files $dbName", (clean_files $folder $cleanupFileAgeDays))
		}
	}

	if ($checkIntegrity -eq 1) {
		Write-Host "`n--------------------------------------------"
		Write-Host "Performing Integrity Checks..."
		Write-Host "--------------------------------------------`n"
		foreach ($db in $dbs) {
			$dbName = $db.Name
			$errorResult.Add("Check Integrity $dbName", (check_table_integ $db))
		}
	}

	if ($indexDefrag -eq 1) {
		Write-Host "`n--------------------------------------------"
		Write-Host "Performing Index Deframentation..."
		Write-Host "--------------------------------------------`n"
		foreach ($db in $dbs) {
			foreach ($table in $db.tables) {
				maint_index_frag $table $sqlVer
			}
		}
	}

	if ($updateStatistics -eq 1) {
		Write-Host "`n--------------------------------------------"
		Write-Host "Performing Update of Statistics..."
		Write-Host "--------------------------------------------`n"
		foreach ($db in $dbs) {
			foreach ($table in $db.tables) {
				update_stats $table
			}
		}
	}

	if ($backupDatabase -eq 1) {
		Write-Host "`n--------------------------------------------"
		Write-Host "Performing Database Backups..."
		Write-Host "--------------------------------------------`n"
		foreach ($db in $dbs) {
			$dbName = $db.Name
			$errorResult.Add("Backup $dbName", (backup_db $svr $db $backupLocation $cleanupFileAgeDays $compression))
		}
	}

	if ($moveFiles -eq 1) {
		Write-Host "`n--------------------------------------------"
		Write-Host "Performing Move of Files to Remote Location..."
		Write-Host "--------------------------------------------`n"
		move_files $moveFileSource $moveFileTarget
	}

	Write-Host "`n--------------------------------------------"
	Write-Host "Error Results..."
	Write-Host "--------------------------------------------`n"
	$errorSubjectLine = $false
	$errorResults.GetEnumerator() | %{
		if ($_.Value -eq $true) {
			$errorName = $_.Name
			Write-Host "$errorName Failed!"
			$errorSubjectLine = $true
		}	
	}

	$svr.ConnectionContext.StatementTimeout = 1200

	Stop-Transcript

}

[string]$from = $sender

[string[]]$to = $recipients

if ($errorSubjectLine -eq $true) {
	[string]$subject = "$Server FAILED! Database Maintenance Tasks"
} else {
	[string]$subject = "$Server PASSED! Database Maintenance Tasks"
}

[string]$body = (gc $log) | out-string

send-mailmessage -from $from -to $to -subject $subject -body $body -smtpServer $email.email.smtpServer


#####################################################################################
#
# Version 1.05
#
# Written on 12/06/2012
# By Michael Pal
# 
#####################################################################################


function load_assemblies {
	param([string]$name)
	$errorLoadAssembly = $false
	
	Write-Host (log_time)"`tLoading .NET SQL Assembly " -nonewline
	Write-Host "$name..." -nonewline
	$timeResult = measure-command {
		if ([System.Reflection.Assembly]::LoadWithPartialName($name)) {
			Write-Host "Completed." -nonewline
		} else {
			Write-Host "Time`tFailed" -nonewline
			$errorLoadAssembly = $true
		}
	}
	Write-Host "$timeResult"
	return $errorLoadAssembly
}

function backup_db {
	param ($server, $database, [string]$backupDir, $days, [bool]$compression)
	
	$errorBackup = $false
	$timestamp = Get-Date -Format yyyyMMddHHmmss
	$recoverymod = $database.DatabaseOptions.RecoveryModel
	$dbName = $database.Name
	
	if (!(test-path "$backupDir\$dbname")) {
		New-Item -Path "$backupDir\$dbname" -Type Directory
	}
	$backupFile = $backupDir + "\" + $dbname + "\" + $dbName + "_db_" + $timestamp + ".bak"
		
	Write-Host (log_time)"`tCreating Sql Server Backup Object ..." -nonewline	
	$timeResult = measure-command { $smoBackup = New-Object -TypeName Microsoft.SqlServer.Management.SMO.Backup }
	Write-Host "Completed. $timeResult"
	
	$smoBackup.Action = [Microsoft.SqlServer.Management.SMO.BackupActionType]::Database
	
	$smoBackup.BackupSetDescription = "Full Backup of " + $dbName
	$smoBackup.BackupSetName = "$dbName-$timestamp-Backup"
	$smoBackup.Database = $dbName
	$smoBackup.MediaDescription = "Disk"
	
	if ($compression) {
		$smoBackup.CompressionOption = [Microsoft.SqlServer.Management.Smo.BackupCompressionOptions]::On
	}
	
	$deviceType = [Microsoft.SqlServer.Management.SMO.DeviceType]::File
	$backupDeviceItem = New-Object -TypeName Microsoft.SqlServer.Management.SMO.BackupDeviceItem -argumentlist $backupFile, $deviceType
	$smoBackup.Devices.Add($backupDeviceItem)
	
	$smoBackup.Incremental = $false
	$smoBackup.ExpirationDate = (get-date).AddDays($days)
	$smoBackup.LogTruncation = [Microsoft.SqlServer.Management.SMO.BackupTruncateLogType]::Truncate
	
	Write-Host (log_time)"`tBacking up Database $database to $backupFile..." -nonewline
	$timeResult = measure-command { $smoBackup.SqlBackup($server) }
	Write-Host "Completed. $timeResult."
	
	$smoBackup.Devices.Remove($backupDeviceItem) | out-null
	
	if ((test-Path $backupFile) -eq $true) {
		Write-Host (log_time)"`t$backupFile Exists..."
	} else {
		Write-Host (log_time)"`tERROR! $backupFile does not Exists..."
		$errorBackup = $true
	}
	
	return $errorBackup
}

function update_stats {
	param ($table)
		
	Write-Host (log_time)"`tUpdating table statistics for $table."
	$table.UpdateStatistics("all","percent", 50)
	Write-Host (log_time)"`tCompleted updating table statistics for $table."
}

function maint_index_frag {
	param ($table,$sqlVer)
	
	$reorganized = 0
	$pageSize = 100
	
	foreach ($index in $table.Indexes) {
		$tableIndexes = $index.EnumFragmentation()
		$tableIndexes | %{
			$Index_Name = $_.Index_Name;
			if ([float]$sqlVer -lt 9.0) {
				$avgFrag = $_.LogicalFragmentation
			} else {
				$avgFrag = $_.AverageFragmentation
			}
			
			if ($avgFrag -ge 30.00 -AND $_.Pages -gt $pageSize) {
				Write-Host (log_time)"`t" -nonewline
				$_ | FT  @{ Label="ID"; Expression={$_.Index_ID}}, @{ Label="Index Name"; Expression={$_.Index_Name}; width=20 }, @{ Label="Type"; Expression={$_.Index_Type} },Pages, @{ Label="Avg Frag"; Expression={"{0:N2}" -f $avgFrag}} -autosize
				$avgFragNum = "{0:N2}%" -f $avgFrag
				if ($index.Name -notmatch "syscolumns" -AND $index.Name -notmatch "syscomments") {
					Write-Host (log_time)"`t$Index_Name is $avgFragNum% fragmented and will be rebuilt."
					$timeResult = measure-command { $index.Rebuild() }
					Write-Host (log_time)"`t$Index_Name has been rebuilt in $timeResult."
				}
			}
			if ($avgFrag -gt 10.00 -AND $avgFrag -lt 30.00 -AND $_.Pages -ge $pageSize) {
				$_ | FT  @{ Label="ID"; Expression={$_.Index_ID}}, @{ Label="Index Name"; Expression={$_.Index_Name}; width=20 }, @{ Label="Type"; Expression={$_.Index_Type} },Pages, @{ Label="Avg Frag"; Expression={"{0:N2}" -f $_.AverageFragmentation}} -autosize
				$avgFragNum = "{0:N2}%" -f $avgFrag
				if ($index.Name -notmatch "syscolumns" -AND $index.Name -notmatch "syscomments") {
					Write-Host (log_time)"`t$Index_Name is $avgFragNum% fragmented and will be reorganized."
					$timeResult = measure-command { $index.Reorganize() }
					$reorganized = 1
					Write-Host (log_time)"`t$Index_Name has been reorganized in $timeResult."
				}
			}
			if ($avgFrag -gt 0.00 -AND $avgFrag -le 10.00 -AND $_.Pages -ge $pageSize) {
				$avgFragNum = "{0:N2}%" -f $avgFrag
				Write-Host (log_time)"`t$Index_Name is $avgFragNum fragmented. No action is required."
			}
		}
	}
	if ($reorganized -eq 1) {
		update_stats $table
	}
}

function check_table_integ {
	param($db)
	
	$errorIntegrity = $false
	$timeResult = measure-command { $sc = $db.CheckTables([Microsoft.SqlServer.Management.SMO.RepairType]::None,[Microsoft.SqlServer.Management.Smo.RepairOptions]::AllErrorMessages) }
	$dbName = $db.Name
	$scResult = $sc.item($sc.count -2)
	[int]$checkAllocation = (($sc.item($sc.count -2)).split(" "))[2]
	[int]$checkConsistency = (($sc.item($sc.count -2)).split(" "))[6]
	
	if ($checkAllocation -eq 0 -AND $checkConsistency -eq 0) {
		write-host (log_time)"`t$dbName Passed Integrity Checks. $timeResult"
		write-host (log_time)"`t$scResult"
	}
	if ($checkAllocation -ne 0) {
		write-host (log_time)"`t$dbName FAILED Integrity Checks. $timeResult"
		write-host (log_time)"`t$scResult"
		$errorIntegrity = $true
	}
	if ($checkConsistency -ne 0) {
		write-host (log_time)"`t$dbName FAILED Integrity Checks. $timeResult"
		write-host (log_time)"`t$scResult"
		$errorIntegrity = $true
	}
	
	return $errorIntegrity
}